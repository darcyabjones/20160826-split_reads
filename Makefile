C=$(shell pwd)
SPACE :=
SPACE +=
COMMA := ,
NCPU=4


# Change the following parameters to match your own configuration.
DATA=data # The directory where the base data are stored
GENOME_NAME=SN15 # This controls the output names from BBsplit
GENOME_FILE=$(DATA)/Parastagonospora_nodorum_SN15_scaffolds.fasta # Genome fasta
READS1_PATTERN=-R1.fastq.gz
READS2_PATTERN=-R2.fastq.gz
READS1=$(shell ls $(DATA)/*$(READS1_PATTERN))
READS2=$(shell ls $(DATA)/*$(READS2_PATTERN))
#READS1=data/invitro-SN15-000-R1.fastq.gz # for debugging
#READS2=data/invitro-SN15-000-R2.fastq.gz
SAMPLE_NAMES=$(subst $(READS1_PATTERN),,$(notdir $(READS1)))

BBMAP_DOCKER=
CUTADAPT_DOCKER=
# Uncomment to run inside docker container. I can't guarantee it'll work though.
#BBMAP_DOCKER=sudo docker run --rm -v $$PWD:/data:z darcyabjones/bbmap
#CUTADAPT_DOCKER=sudo docker run --rm -v $$PWD:/data:z darcyabjones/cutadapt


three_read1="AGATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNATCTCGTATGCCGTCTTCTGCTTG"
three_read2="AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT"
five_read1="AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT"
five_read2="GTTCGTCTTCTGCCGTATGCTCTANNNNNNCACTGACCTCAAGTCTGCACACGAGAAGGCTAG"

ADAPTER_DIR=trim_adapters # Directory where adapter trimmed reads will be stored.
ADAPTER_EXTS=-R1-atrimmed.fastq.gz -R2-atrimmed.fastq.gz -atrimmed.log
ADAPTER_TARGETS=$(foreach e, $(ADAPTER_EXTS), $(addprefix $(ADAPTER_DIR)/, $(addsuffix $(e), %)))
ADAPTER_FILES=$(foreach s, $(SAMPLE_NAMES), $(subst %,$(s), $(ADAPTER_TARGETS)))

SPLIT_DIR=split # Directory where bbsplit results will be stored.
SPLIT_EXTS=-R1-atrimmed-$(GENOME_NAME).fastq.gz -R2-atrimmed-$(GENOME_NAME).fastq.gz -R1-atrimmed-unmapped.fastq.gz -R2-atrimmed-unmapped.fastq.gz -atrimmed-refstats.tsv -atrimmed-scafstats.tsv
SPLIT_TARGETS=$(foreach e, $(SPLIT_EXTS), $(addprefix $(SPLIT_DIR)/, $(addsuffix $(e), %)))
SPLIT_FILES=$(foreach s, $(filter inplanta-%, $(SAMPLE_NAMES)), $(subst %,$(s), $(SPLIT_TARGETS)))

QUALITY_DIR=trim_quality # Directory where quality trimmed reads will be stored.
QUALITY_EXTS=-R1-atrimmed-unmapped-qtrimmed.fastq.gz -R2-atrimmed-unmapped-qtrimmed.fastq.gz -unmapped-atrimmed-qtrimmed.log
QUALITY_TARGETS=$(foreach e, $(QUALITY_EXTS), $(addprefix $(QUALITY_DIR)/, $(addsuffix $(e), %)))
QUALITY_FILES=$(foreach s, $(filter inplanta-%, $(SAMPLE_NAMES)), $(subst %,$(s), $(QUALITY_TARGETS)))

# Phony is really just used for debugging.
phony:
	@echo $(ADAPTER_FILES)
	@echo $(SPLIT_FILES)
	@echo $(QUALITY_FILES)

# Do the actual work
cleanall: all clean
	@rm -rf $^

clean:
	@rm -rf index

all: trim_adapters split trim_quality

trim_adapters: $(ADAPTER_FILES)
split: $(SPLIT_FILES)
trim_quality: $(QUALITY_FILES)

# Adapter trim reads
$(ADAPTER_TARGETS): $(DATA)/%-R1.fastq.gz $(DATA)/%-R2.fastq.gz
	@mkdir -p $(dir $@)
	$(CUTADAPT_DOCKER) cutadapt \
		--minimum-length 25 \
		-a $(three_read1) \
		-A $(three_read2) \
		-g $(five_read1) \
		-G $(five_read2) \
		-n 3 \
		-o $(dir $@)/$(subst .fastq.gz,,$(notdir $(word 1, $^)))-atrimmed.fastq.gz \
		-p $(dir $@)/$(subst .fastq.gz,,$(notdir $(word 2, $^)))-atrimmed.fastq.gz \
		$(word 1, $^) $(word 2, $^) \
		> $(dir $@)/$(subst $(READS1_PATTERN),,$(notdir $(word 1, $^)))-atrimmed.log

# Split reads by the genome
# NB the # in the output file names are special characters for bbsplit 
$(SPLIT_TARGETS): $(ADAPTER_DIR)/%-R1-atrimmed.fastq.gz $(ADAPTER_DIR)/%-R2-atrimmed.fastq.gz
	@mkdir -p $(dir $@)
	$(BBMAP_DOCKER) bbsplit.sh \
		-Xmx30g \
		ambiguous=all \
		path=index \
		ref_SN15=$(GENOME_FILE) \
		out_SN15=$(dir $@)/$(subst -R1-atrimmed.fastq.gz,,$(notdir $(word 1, $^)))-R#-atrimmed-$(GENOME_NAME).fastq.gz \
		outu=$(dir $@)/$(subst -R1-atrimmed.fastq.gz,,$(notdir $(word 1, $^)))-R#-atrimmed-unmapped.fastq.gz \
		in=$(word 1, $^) \
		in2=$(word 2, $^) \
		refstats=$(dir $@)/$(subst -R1-atrimmed.fastq.gz,,$(notdir $(word 1, $^)))-atrimmed-refstats.tsv \
		scafstats=$(dir $@)/$(subst -R1-atrimmed.fastq.gz,,$(notdir $(word 1, $^)))-atrimmed-scafstats.tsv

# Quality trim the reads. We didn't actually end up using these reads.
# Instead we used soft trimmed alignments.
# Done in 3 passes to get high quality reads.
$(QUALITY_TARGETS): $(SPLIT_DIR)/%-R1-atrimmed-unmapped.fastq.gz $(SPLIT_DIR)/%-R2-atrimmed-unmapped.fastq.gz
	@mkdir -p $(dir $@)
	@echo "# Cut 1" > $(dir $@)/$(subst -R1-atrimmed-unmapped.fastq.gz,,$(notdir $(word 1, $^)))-atrimmed-unmapped-qtrimmed.log
	$(CUTADAPT_DOCKER) cutadapt \
	  --quality-cutoff=30 \
		--minimum-length 25 \
		-a $(three_read1) \
		-A $(three_read2) \
		-g $(five_read1) \
		-G $(five_read2) \
		-n 3 \
		-o $(dir $@)/$(subst .fastq.gz,,$(notdir $(word 1, $^)))-qtrimmed.tmp1.fastq \
		-p $(dir $@)/$(subst .fastq.gz,,$(notdir $(word 2, $^)))-qtrimmed.tmp1.fastq \
		$(word 1, $^) $(word 2, $^) \
	  >> $(dir $@)/$(subst -R2-atrimmed-unmapped.fastq.gz,,$(notdir $(word 1, $^)))-atrimmed-unmapped-qtrimmed.log
	@echo "# Cut 2" >> $(dir $@)/$(subst -R2-atrimmed-unmapped.fastq.gz,,$(notdir $(word 1, $^)))-atrimmed-unmapped-qtrimmed.log
	$(CUTADAPT_DOCKER) cutadapt \
	  --quality-cutoff=30 \
		--minimum-length 25 \
		-a $(three_read1) \
		-A $(three_read2) \
		-g $(five_read1) \
		-G $(five_read2) \
		-n 3 \
		-o $(dir $@)/$(subst .fastq.gz,,$(notdir $(word 1, $^)))-qtrimmed.tmp2.fastq \
		-p $(dir $@)/$(subst .fastq.gz,,$(notdir $(word 2, $^)))-qtrimmed.tmp2.fastq \
		$(dir $@)/$(subst .fastq.gz,,$(notdir $(word 1, $^)))-qtrimmed.tmp1.fastq $(dir $@)/$(subst .fastq.gz,,$(notdir $(word 2, $^)))-qtrimmed.tmp1.fastq
	  >> $(dir $@)/$(subst -R1-atrimmed-unmapped.fastq.gz,,$(notdir $(word 1, $^)))-atrimmed-unmapped-qtrimmed.log
	@rm -f $(dir $@)/*.tmp1.*
	@echo "# Cut 3" >> $(dir $@)/$(subst -R1-atrimmed-unmapped.fastq.gz,,$(notdir $(word 1, $^)))-atrimmed-unmapped-qtrimmed.log
	$(CUTADAPT_DOCKER) cutadapt \
	  --quality-cutoff=30 \
		--minimum-length 25 \
		-a $(three_read1) \
		-A $(three_read2) \
		-g $(five_read1) \
		-G $(five_read2) \
		-n 3 \
		-o $(dir $@)/$(subst .fastq.gz,,$(notdir $(word 1, $^)))-qtrimmed.fastq.gz \
		-p $(dir $@)/$(subst .fastq.gz,,$(notdir $(word 2, $^)))-qtrimmed.fastq.gz \
		$(dir $@)/$(subst .fastq.gz,,$(notdir $(word 1, $^)))-qtrimmed.tmp2.fastq.gz $(dir $@)/$(subst .fastq.gz,,$(notdir $(word 2, $^)))-qtrimmed.fastq.gz.tmp2 \
		>> $(dir $@)/$(subst -R1-atrimmed-unmapped.fastq.gz,,$(notdir $(word 1, $^)))-atrimmed-unmapped-qtrimmed.log
	@rm -f $(dir $@)/*.tmp2.*
